var express = require('express')
var router = express.Router();

var textValidator;

router.post('/validate', function(req, res) {
    var result = textValidator.validate(req.body);
    console.log(result);
    return res.status(200).json(result)
})

module.exports = function(app, tv) {
    textValidator = tv;
    app.use('/', router);
    app.route('/*')
        .get(
            function (req, res) {
                return res.status(500);
            });

}
