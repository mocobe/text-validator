/**
 * Express configuration
 */

'use strict';
module.exports = {
    serve: function(userPort) {
        var express = require('express');
        var bodyParser = require('body-parser');
        var config = require('./config/index');
        if(userPort) { config.port = userPort; }
        var app = express();
        app.use(bodyParser.urlencoded({extended: false}));
        app.use(bodyParser.text());
        var textValidator = require('../')
        require('./routes')(app, textValidator)

        var server = require('http').createServer(app);
        server.listen(config.port, config.ip, function () {
            console.log('Express server listening on %d', config.port);
        });
        return server;
    }
}