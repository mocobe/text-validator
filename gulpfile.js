const gulp = require('gulp');
const exec = require('child_process').exec
gulp.task('default', [], (cb) => {
	exec('node test.js', (err, stdout, stderr) => {
		console.log(stdout);
		console.log(stderr);
		cb(err);
	})
})
gulp.task("_default", ["default"], () => {
	// this task just pushes this up to the top of the sublime-gulp list, so I don't have to type default
	return null;
})
gulp.task('serve', [], (cb) => {
	exec('node -e "var tv = require(\'.\'); tv.serve();"', (err, stdout, stderr) => {
		console.log(stdout);
		console.log(stderr);
		cb(err);
	})
})