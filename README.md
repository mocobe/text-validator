text-validator can be used for validating text inputs in English to make it more difficult for users to skip through forms by simply keyboard mashing.  It performs a variety of simple tests, including sentence length, number of repeats (e.g., "blah blah blah blah"), and comparing words against a database of known words.  It compensates for many simple spelling errors as well, using a simple insertion/deletion/mutation model of spelling.

API:
-
- `validate(sentence)`:
Returns a SentenceTest object.  The isValid property indicates whether the sentence is valid.  The default parameters are very generous.  See examples below for more details.

- `getValidationParams()`:
Get the current set of validation parameters.

- `setValidationParams(params)`:
Set the validation parameters.  The validation parameters object has the following properties by default:
```
{
    gibberishThreshold: 0.5, // max fraction of the sentence that can be unknown words
    maxRepeatPercentage: 0.5, // max fraction of a sentence that can be a single repeated word (e.g. "blah blah blah blah"
    minSentenceLength: 3 // minimum length of a valid sentence in words
}
```
- `serve(port)`:
Serves a rest API on the specified port.  To use the server, POST to the /validate endpoint with the text to be validated in the POST body, and content type text/plain.  It will return a SentenceTest object.

**Examples:**

```

> var tv = require('text-validator');
> tv.validate('i am th ver model of a mdorne major genreal')
SentenceTest {
  matches: 
   [ WordMatch { score: 0, match: 'i', word: 'i' },
     WordMatch { score: 0, match: 'am', word: 'am' },
     WordMatch { score: 1, match: 'ah', word: 'th' },
     WordMatch { score: 1, match: 'eer', word: 'ver' },
     WordMatch { score: 0, match: 'model', word: 'model' },
     WordMatch { score: 0, match: 'of', word: 'of' },
     WordMatch { score: 0, match: 'a', word: 'a' },
     WordMatch { score: -1, match: undefined, word: 'mdorne' },
     WordMatch { score: 0, match: 'major', word: 'major' },
     WordMatch { score: 1, match: 'general', word: 'genreal' } ],
  validateParams: 
   { gibberishThreshold: 0.5,
     maxRepeatPercentage: 0.5,
     minSentenceLength: 3 },
  sentence: 'i am th ver model of a mdorne major genreal',
  words: 
   [ 'i',
     'am',
     'th',
     'ver',
     'model',
     'of',
     'a',
     'mdorne',
     'major',
     'genreal' ],
  wordMatch: 
   [ WordMatch { score: 0, match: 'i', word: 'i' },
     WordMatch { score: 0, match: 'am', word: 'am' },
     WordMatch { score: 1, match: 'ah', word: 'th' },
     WordMatch { score: 1, match: 'eer', word: 'ver' },
     WordMatch { score: 0, match: 'model', word: 'model' },
     WordMatch { score: 0, match: 'of', word: 'of' },
     WordMatch { score: 0, match: 'a', word: 'a' },
     WordMatch { score: -1, match: undefined, word: 'mdorne' },
     WordMatch { score: 0, match: 'major', word: 'major' },
     WordMatch { score: 1, match: 'general', word: 'genreal' } ],
  isValid: true }

> tv.validate('asdf nas hjasdfkln fasdf')
SentenceTest {
  matches: 
   [ WordMatch { score: -1, match: undefined, word: 'asdf' },
     WordMatch { score: 1, match: 'as', word: 'nas' },
     WordMatch { score: -1, match: undefined, word: 'hjasdfkln' },
     WordMatch { score: -1, match: undefined, word: 'fasdf' } ],
  validateParams: 
   { gibberishThreshold: 0.5,
     maxRepeatPercentage: 0.5,
     minSentenceLength: 3 },
  sentence: 'asdf nas hjasdfkln fasdf',
  words: [ 'asdf', 'nas', 'hjasdfkln', 'fasdf' ],
  wordMatch: 
   [ WordMatch { score: -1, match: undefined, word: 'asdf' },
     WordMatch { score: 1, match: 'as', word: 'nas' },
     WordMatch { score: -1, match: undefined, word: 'hjasdfkln' },
     WordMatch { score: -1, match: undefined, word: 'fasdf' } ],
  isValid: false }

  > tv.validate('twas brillig and the slithy toves did gyre and gimble in the wabe')
SentenceTest {
  matches: 
   [ WordMatch { score: 1, match: 'was', word: 'twas' },
     WordMatch { score: -1, match: undefined, word: 'brillig' },
     WordMatch { score: 0, match: 'and', word: 'and' },
     WordMatch { score: 0, match: 'the', word: 'the' },
     WordMatch { score: 1, match: 'smithy', word: 'slithy' },
     WordMatch { score: 1, match: 'toes', word: 'toves' },
     WordMatch { score: 0, match: 'did', word: 'did' },
     WordMatch { score: 1, match: 'lyre', word: 'gyre' },
     WordMatch { score: 0, match: 'and', word: 'and' },
     WordMatch { score: 1, match: 'nimble', word: 'gimble' },
     WordMatch { score: 0, match: 'in', word: 'in' },
     WordMatch { score: 0, match: 'the', word: 'the' },
     WordMatch { score: 1, match: 'abe', word: 'wabe' } ],
  validateParams: 
   { gibberishThreshold: 0.5,
     maxRepeatPercentage: 0.5,
     minSentenceLength: 3 },
  sentence: 'twas brillig and the slithy toves did gyre and gimble in the wabe',
  words: 
   [ 'twas',
     'brillig',
     'and',
     'the',
     'slithy',
     'toves',
     'did',
     'gyre',
     'and',
     'gimble',
     'in',
     'the',
     'wabe' ],
  wordMatch: 
   [ WordMatch { score: 1, match: 'was', word: 'twas' },
     WordMatch { score: -1, match: undefined, word: 'brillig' },
     WordMatch { score: 0, match: 'and', word: 'and' },
     WordMatch { score: 0, match: 'the', word: 'the' },
     WordMatch { score: 1, match: 'smithy', word: 'slithy' },
     WordMatch { score: 1, match: 'toes', word: 'toves' },
     WordMatch { score: 0, match: 'did', word: 'did' },
     WordMatch { score: 1, match: 'lyre', word: 'gyre' },
     WordMatch { score: 0, match: 'and', word: 'and' },
     WordMatch { score: 1, match: 'nimble', word: 'gimble' },
     WordMatch { score: 0, match: 'in', word: 'in' },
     WordMatch { score: 0, match: 'the', word: 'the' },
     WordMatch { score: 1, match: 'abe', word: 'wabe' } ],
  isValid: true }

> tv.validate('O frabjous day! Callooh! Callay!')
SentenceTest {
  matches: 
   [ WordMatch { score: -1, match: undefined, word: 'o' },
     WordMatch { score: -1, match: undefined, word: 'frabjous' },
     WordMatch { score: 0, match: 'day', word: 'day' },
     WordMatch { score: -1, match: undefined, word: 'callooh' },
     WordMatch { score: 1, match: 'allay', word: 'callay' } ],
  validateParams: 
   { gibberishThreshold: 0.5,
     maxRepeatPercentage: 0.5,
     minSentenceLength: 3 },
  sentence: 'o frabjous day callooh callay ',
  words: [ 'o', 'frabjous', 'day', 'callooh', 'callay' ],
  wordMatch: 
   [ WordMatch { score: -1, match: undefined, word: 'o' },
     WordMatch { score: -1, match: undefined, word: 'frabjous' },
     WordMatch { score: 0, match: 'day', word: 'day' },
     WordMatch { score: -1, match: undefined, word: 'callooh' },
     WordMatch { score: 1, match: 'allay', word: 'callay' } ],
  isValid: false }
 ```