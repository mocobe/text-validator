/// <reference path="./typings/tsd.d.ts" />
/// <reference path="./typings/node/node.d.ts" />
'use strict';
var _ = require('lodash');
var yaml = require('js-yaml');
var fs = require('fs');
var config = yaml.load(fs.readFileSync(__dirname + '/config.yml'));
class EditFunctions  {
    //var alphabet = _.range(26).map(function (n) { return String.fromCharCode(97 + n); });
    constructor() {
    }

    static deletes(w) {
        return _.range(0, w.length).map(function (n) { return w.substring(0, n) + w.substring(n + 1, w.length); });
    };
    static transpositions(w) {
        return _.range(0, w.length - 1).map(function (n) { return w.substring(0, n) + w[n + 1] + w[n] + w.substring(n + 2, w.length); });
    };
    static replaces(w) {
        return _.flatten(_.range(0, w.length).map(function (n) {
            var letter = w[n];
            return _.filter(EditFunctions.alphabet, function (c) { return c != w[n]; })
                .map(function (c) {
                    return w.substring(0, n) + c + w.substring(n + 1, w.length);
                });
        }));
    };
    static inserts(w) {
        return _.flatten(_.range(1, w.length).map(function (n) {
            return _.map(EditFunctions.alphabet, function (c) {
                return w.substring(0, n) + c + w.substring(n, w.length);
            });
        }));
    };
    static allSingleEdits(w) {
        return EditFunctions.arrayToHashTable(_.concat(EditFunctions.deletes(w), EditFunctions.transpositions(w), EditFunctions.replaces(w), EditFunctions.inserts(w)));
    };
    static allDoubleEdits(w) {
        var singleEdits = EditFunctons.allSingleEdits(w);
        return EditFunctions.arrayToHashTable(_.flatten(_.map(singleEdits, function (w, v) { return EditFunctions.allSingleEdits(w); })));
    };
    static arrayToHashTable(words) {
        var res = {};
        _.forEach(words, function (w) { res[w] = true; });
        return res;
    };
}

EditFunctions.alphabet = _.range(26).map(function (n) { return String.fromCharCode(97 + n); });

class WordMatch {
    constructor(w, s, m) {
        this.score = s;
        this.match = m;
        this.word = w;
    }
};

class WordTest {
    constructor(config) {
        var _this = this;
        //	this.wordModel = WordModel.generateWordModel(config.freq);
        this.dict = {};
        _.forEach(config.dictfiles, function (dictFile) {
            _.forEach(fs.readFileSync(__dirname + "/" + config.datadir + "/" + dictFile, { encoding: 'UTF-8' }).split("\n"), function (w) { _this.dict[w] = true; });
        });
    }

    isWord(w) {
        return this.dict[w];
    };
    match(w) {
        var _this = this;
        if (this.dict[w]) {
            return new WordMatch(w, 0, w);
        }
        var edits1 = EditFunctions.allSingleEdits(w);
        var corrected;
        if (corrected = _.findKey(edits1, function (value, key) { return _this.dict[key]; })) {
            return new WordMatch(w, 1, corrected);
        }
        var edits2 = _.flatten(_.map(edits1, function (b, w) { return EditFunctions.allSingleEdits(w); }));
        if (corrected = _.findKey(edits2, function (value, key) { return _this.dict[key]; })) {
            return new WordMatch(w, 2, corrected);
        }
        return new WordMatch(w, -1, undefined);
    };

    sentence(s) {
        var _this = this;
        return _.map(s, function (w) {
            return _this.match(w);
        });
    };

}

class SentenceTest {
    constructor(wordTest, s, validateParams) {
        this.matches = null;
        this.validateParams = config.validateParams;
        //this.matches = wordTest.sentence(s);
        this.wordTest = wordTest;
        this.sentence = s.toLowerCase().replace(/([^\w\s']|\d|_)+/g, " ").replace(/\s+/g, " ")
        this.words = _.map(this.sentence.split(/\s+/), function (w) { return w.replace(/\'/, ""); })
            .filter(function(w) { return w != ''; })
        this.wordMatch = this.getWordMatch();
        this.isValid = this.validate();
    }

    sentenceLength() {
        return this.words.length;
    };

    // the number of times the most repeated word appears e.g. "foo foo foo bar bar quux" will return 3
    maxRepeat() {
        return _(this.words).countBy(_.identity).toArray().max();
    };

    // lazily initialize in case we don't need it
    getWordMatch() {
        if (!this.matches) {
            this.matches = this.wordTest.sentence(this.words);
        }
        return this.matches;
    };

    unmatchedWords() {
        var unmatchedWords = _.filter(this.wordMatch, function (m) { return m.score < 0; });
        return { num: unmatchedWords.length, fraction: unmatchedWords.length / this.sentenceLength() };
    };

    validate() {
        if(this.sentenceLength() < this.validateParams.minSentenceLength) { return false; }
        if(this.maxRepeat()/this.sentenceLength() > this.validateParams.maxRepeatPercentage) { return false; }
        if(this.unmatchedWords().fraction > this.validateParams.gibberishThreshold) { return false; }
        return true;
    }

}

class SentenceValidator {
    constructor(wordTest, config) {
        this.wordTest = wordTest;
        this.config = config;
        this.server = require('./server')
        this.result = null;
    }

    validate(sentence) {
        var result = new SentenceTest(this.wordTest, sentence, this.config.validateParams);
        delete result.wordTest;
        delete result.gibberishThreshold;
        return result;
    }

    serve(port) {
        return this.server.serve(port);
    }

    getValidationParams() {
        return this.config.validateParams;
    }

    setConfig(p) {
        this.config.validateParams = p;
    }

}
var WORD_TEST = new WordTest(config);
var SENTENCE_VALIDATOR = new SentenceValidator(WORD_TEST, config)
module.exports = SENTENCE_VALIDATOR;